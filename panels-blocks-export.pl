#!/usr/bin/perl

use 5.012;
use strict;
use warnings;
use DBI;
use Data::Dumper;
use YAML::XS;
use JSON::XS;
use Getopt::Long;
use Digest::MD5 qw(md5);
use Config::YAML;
use File::Path qw(make_path);

my $dbattr = {
  PrintError => 1,
  RaiseError => 0,
  AutoCommit => 0,
};

my $outdir = "$ENV{HOME}/drupal-export";

# use 'block'
my $module = "block";

my $url_prefix = "";

# You can define your own YAML-based config and specify below.
my $c = Config::YAML->new( config => "/usr/share/drupal-data-export/global.yaml",
                           outdir => $outdir,
                           module => $module,
                           prefix => $url_prefix );

GetOptions ( "outdir|O=s" => \$outdir,
             "module|m=s" => \$module,
             "prefix|p=s" => \$url_prefix
           )
  or die ("Error in command line arguments\n");

my ($schema) = @ARGV;

# append 'schema' to the outdir
# $outdir .= "/" . $schema;
say "outdir=[" . $outdir . "]";
make_path( $outdir ) unless -e $outdir;
#mkdir $outdir unless -e $outdir;

if ($url_prefix ne '') {
  $url_prefix .= "/" if $url_prefix !~ /\/$/;
}

# Override the default config value db.schema
# when the value is available from the CLI
$c->{db}->{schema} = $schema unless !defined $schema;

my $dsn = sprintf("dbi:%s:%s;host=%s;port=%s", 
  $c->{db}->{driver},
  $c->{db}->{schema}, 
  $c->{db}->{host},
  $c->{db}->{port});

my $db = DBI->connect($dsn, $c->{db}->{user}, $c->{db}->{password}, $dbattr)
  or die "Can't connect, yo!";

$db->{mysql_enable_utf8} = 1;

# QUERIES SECTION
# --------------------------
my $pm_pages_sth = $db->prepare(
  "SELECT name, task, admin_title, admin_description, path "
  . "FROM page_manager_pages"
);

my $pm_handler_sth = $db->prepare("SELECT name, task, subtask, handler, weight "
  . "FROM page_manager_handlers "
  . "WHERE task = ? and subtask = ?"
);

my $pm_display_sth = $db->prepare("SELECT did, layout, layout_settings, panel_settings, "
  . "cache, title, hide_title, title_pane, uuid, storage_type, storage_id "
  . "FROM panels_display "
  . "WHERE storage_id = ?"
);

my $pm_panes_sth = $db->prepare("SELECT * FROM panels_pane WHERE did = ? AND shown = 1");

# FILE/DIRECTORY stuff
# ensure the 'node' directory is present and empty 
# before saving files.
mkdir "$outdir/page_manager" if !-e "$outdir/page_manager";
`rm -f $outdir/page_manager/*` if -e "$outdir/page_manager";

# configure our JSON object to:
# - print "pretty" structured output
# - with sorted keys
# - enforce UTF8
my $json = JSON::XS->new->pretty(1)->utf8->space_after->canonical(1);

my $pages_exported = 0;

# QUERY EXECUTION and PROCESSING
# ==============================
$pm_pages_sth->execute;
my $pm_pages = $pm_pages_sth->fetchall_arrayref({});
foreach my $page (@{$pm_pages}) {
  my $page_title = $page->{admin_title} ? $page->{admin_title} : $page->{name};
  say "processing " . $page_title . "...";

  # construct path alias...
  my $path_alias = '/' . $url_prefix . $page->{path};

  #if (@index_pages) {
  #  my @s = grep{ $_ eq $path_alias } @index_pages;
  #  if (@s) {
  #   $path_alias = '/' . $url_prefix;
  #    $path_alias =~ s/\/$//;
  #    say "Using 'landing page' index path == [" . $path_alias . "]";
  #  }
  #}

  my $node = {
    #'path_alias' => { 'alias' => '/' . $url_prefix . $page->{path} },
    'path_alias' => { 'alias' => $path_alias },
    'node_field_data' => {
      'title' => '[Panel Page] ' . $page_title,
      'type' => 'page',
      'sticky' => 0,
      'promote' => 0,
    },
    'node__body' => {
      'body_value' => "The content for this node is accessible in its layout section (coming soon!)",
      'body_format' => "full_html",
    },
    'section_components' => [],
  };
  $pm_handler_sth->execute($page->{task}, $page->{name});
  my $handler = $pm_handler_sth->fetchrow_hashref;

  #say "";
  #say "Processing handler...";
  #say Dumper $handler;

  $pm_display_sth->execute($handler->{name});
  my $display = $pm_display_sth->fetchrow_hashref;

  # replace the title if provided by the "display" record
  if ($display->{title} && ($display->{title} ne '')) {
  	$node->{node_field_data}->{title} = "[Panel Page] " . $display->{title};
  }

  # now that we have the display, we can fetch the panes and create JSON content
  $pm_panes_sth->execute($display->{did});

  my $panes = $pm_panes_sth->fetchall_arrayref({});
  my $weight = 0;
  foreach my $pane (@{$panes}) {
    # create block object
    my $new_component = {
      'uuid' => $pane->{uuid},
      'configuration' => {
        'view_mode' => 'full'
      },
      'region' => 'contentmain',
      'weight' => $weight++,
    };

    if ($pane->{type} eq 'custom') {
      $new_component->{configuration}->{provider} = 'layout_builder';
      $new_component->{configuration}->{id} = 'inline_block:basic';

      # save the block_content so it can be (re)created by the 
      # ingest script
      my $b = {
        'uuid' => $pane->{uuid},
        'content' => {
          # the import script will need to unserialize this string
          # to get at the 'body', 'format', 'admin_title' attributes
          'configuration' => $pane->{configuration},
          'langcode' => 'en',
          # the import script will need to verify this UUID
          # (to avoid collision)
          'uuid' => $pane->{uuid},

          'type' => 'basic',
          'reusable' => 0,
        }
      };
      $new_component->{block_content} = $b;

    } elsif ($pane->{type} eq 'block') {
      $new_component->{configuration}->{provider} = 'block_content';
      $new_component->{configuration}->{id} = 'block_content:%UUID%';
      $new_component->{pane_subtype} = $pane->{subtype};
      # need to keep track of the block's id (bid)
      $new_component->{storage_bid} = $pane->{subtype} =~ s/block-//r;
    };

      #say Dumper $b;
      #my $fullpath = sprintf("%s/blocks/pane/block-custom-%s.json", $outdir, $b->{uuid});
      #say $fullpath;
      #open my $fh, '>', $fullpath or die "Unable to open file $fullpath: $!\n";
      #print $fh $json->encode( $b );
      #$blocks_exported++;

    # now add this
    #say "---";
    #say Dumper $new_component;
    #say "***";
    push @{$node->{section_components}}, $new_component;
    # say Dumper $node->{sections};
  }

  #say Dumper $node;
  my $fullpath = sprintf("%s/page_manager/%s.json", $outdir, $page->{name});
  say $fullpath;
  open my $fh, '>', $fullpath or die "Unable to open file $fullpath: $!\n";
  print $fh $json->encode($node);
  $pages_exported++
}

say sprintf("%s page_manager pages exported...", $pages_exported);
$pm_pages_sth->finish;
$pm_handler_sth->finish;
$pm_display_sth->finish;

$db->disconnect; 
__END__
