#!/usr/bin/perl

use 5.012;
use strict;
use warnings;
use DBI;
use Data::Dumper;
use YAML::XS;
use JSON::XS;
use Getopt::Long;
use Digest::MD5 qw(md5);
use Config::YAML;
use File::Path qw(make_path);

my $dbattr = {
  PrintError => 1,
  RaiseError => 0,
  AutoCommit => 0,
};

my $outdir = "$ENV{HOME}/drupal-export";

# use 'rubenstein/', 'lilly/', etc
my $url_prefix = "";

# You can define your own YAML-based config and specify below.
my $c = Config::YAML->new( config => "/usr/share/drupal-data-export/global.yaml",
                           outdir => $outdir,
                           prefix => $url_prefix );

GetOptions ( "outdir|O=s" => \$outdir,
             "prefix|p=s" => \$url_prefix )
  or die ("Error in command line arguments\n");

my ($schema) = @ARGV;

# append 'schema' to the outdir
# $outdir .= "/" . $schema;
say "outdir=[" . $outdir . "]";
make_path( $outdir ) unless -e $outdir;
#mkdir $outdir unless -e $outdir;

# Override the default config value db.schema
# when the value is available from the CLI
$c->{db}->{schema} = $schema unless !defined $schema;

my $dsn = sprintf("dbi:%s:%s;host=%s;port=%s", 
  $c->{db}->{driver},
  $c->{db}->{schema}, 
  $c->{db}->{host},
  $c->{db}->{port});

my $db = DBI->connect($dsn, $c->{db}->{user}, $c->{db}->{password}, $dbattr)
  or die "Can't connect, yo!";

$db->{mysql_enable_utf8} = 1;

# Fetch and export all of the known taxonomy terms
# including the config name for the assigned vocabulary
my $terms_sth = $db->prepare("SELECT t.*, v.machine_name as vocab_machine_name "
    . "FROM taxonomy_term_data t "
    . "LEFT JOIN taxonomy_vocabulary v ON v.vid = t.vid"
    );
$terms_sth->execute;
my $terms = $terms_sth->fetchall_arrayref({});

# ensure the 'node' directory is present and empty 
# before saving files.
mkdir "$outdir/taxonomy" if !-e "$outdir/taxonomy";
mkdir "$outdir/taxonomy/terms" if !-e "$outdir/taxonomy/terms";
`rm $outdir/taxonomy/terms/*` if -e "$outdir/taxonomy/terms";

my $json = JSON::XS->new->pretty(1)->utf8->space_after;

foreach my $t (@{$terms}) {
  say $t->{uuid} . ".json";
  my $fullpath = sprintf("%s/taxonomy/terms/%s.json", $outdir, $t->{uuid});
  open my $fh, '>', $fullpath or die "Unable to open file $fullpath: $!\n";
  print $fh $json->encode( { term => $t } );
  #print $fh Dump { term => $t };
}

my $vocab_sth = $db->prepare("SELECT * FROM taxonomy_vocabulary WHERE machine_name != 'tags'");
my $term_data_sth = $db->prepare("SELECT * FROM taxonomy_term_data WHERE vid = ?");

# Prepare the vocabulary items for export
$vocab_sth->execute();
my $vocab =  $vocab_sth->fetchall_arrayref({});
foreach my $v (@{$vocab}) {
  $v->{vid} = sprintf("taxonomy.vocabulary.%s", $v->{machine_name});
}

my $fullpath = sprintf("%s/taxonomy/vocabulary.json", $outdir);
open my $fh, '>', $fullpath or die "Unable to open file $fullpath: $!\n";
#print $fh Dump { vocabulary => $vocab };
print $fh $json->encode( { vocabulary => $vocab } );

$db->disconnect; 
__END__
