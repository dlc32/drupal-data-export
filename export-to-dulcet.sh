#!/bin/bash

# or creating export files to our 'dulcet' project folder
./node-export.pl -O ~/Workspace/dulcet/drupal7-export/perkins perkins
./node-export.pl -O ~/Workspace/dulcet/drupal7-export/rubenstein -p rubenstein rubenstein
./node-export.pl -O ~/Workspace/dulcet/drupal7-export/datavis -p data datagis
./node-export.pl -O ~/Workspace/dulcet/drupal7-export/east/lilly -F lilly east
./node-export.pl -O ~/Workspace/dulcet/drupal7-export/east/music -F music east

# export panel pages (don't use pagemgr.pl)
./panels-blocks-export.pl -O ~/Workspace/dulcet/drupal7-export/perkins perkins
./panels-blocks-export.pl -O ~/Workspace/dulcet/drupal7-export/rubenstein -p rubenstein rubenstein
./panels-blocks-export.pl -O ~/Workspace/dulcet/drupal7-export/datavis -p data datagis
./panels-blocks-export.pl -O ~/Workspace/dulcet/drupal7-export/east east

./block-export.pl -O ~/Workspace/dulcet/drupal7-export/perkins perkins
./block-export.pl -O ~/Workspace/dulcet/drupal7-export/rubenstein rubenstein
./block-export.pl -O ~/Workspace/dulcet/drupal7-export/datavis datagis
./block-export.pl -O ~/Workspace/dulcet/drupal7-export/east east

