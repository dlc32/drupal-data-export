# Drupal Data Export Project (for DUL)

## Purpose
This collection of PERL scripts attempts to generate JSON files from the following Drupal data from DUL's Drupal 7 instances:
* Node (*including path_alias, related image*)
* File Managed
* Taxonomy
  
The intended Drupal 7 instance is our (DUL) Drupal instance. Due to that, this script would likely require 
modifications if used on another Drupal 7 instance.

## Configuration
Create the following file, `/usr/share/drupal-data-export/global.yaml` and add the following content:
```yaml
db:
  driver: mysql
  host: localhost
  port: 3306
  user: <your-username>
  password: <your-password>
  schema: <your-default-schema>
```

## Exporting Drupal 7 Data to YAML Files

Each script in the directory takes the same arguments and options.  
  
*Usage*:
```sh
./node-export.pl|taxonomy-export.pl|file-managed-export.pl [options] schema

'schema' is the database where your Drupal tables reside.

OPTIONS:
-O, --outdir
  Location to dump exported JSON files.

-p, --prefix
  Multisite "URL Prefix" (e.g. "rubenstein") used to organize content.

-F, --filter-prefix
  Filter nodes by the first portion of their url_alias (when present)
```
Example:
```sh
./node-export.pl -O yaml/perkins perkins
./node-export.pl -O yaml/east/lilly -F lilly east

# or creating export files to our 'dulcet' project folder
./node-export.pl -O ~/Workspace/dulcet/drupal7-export/perkins perkins

# export panel pages (don't use pagemgr.pl)
./panels-blocks-export.pl -O ~/Workspace/dulcet/drupal7-export/perkins perkins

./block-export.pl -O ~/Workspace/dulcet/drupal7-export/perkins perkins
```
Each of the scripts assumes `localhost` is the database host.  

### Node-specific
`node` directory will be created, or have its contents deleted.

### Taxonomy-specific
`taxonomy` directory will be created.  
`taxonomy/terms` directory will be created, or have its contents deleted.

### File Managed (managed files)
`file_managed` directory will be created, or have its contents deleted.

## Data Import Process to Drupal 9

### Export Data Structure - Blocks
```json
{
   "content" : {
      "uuid" : "%UUID%",
      "info" : "Your Block Title",
      "langcode" : "en",
      "type" : "basic",
      "body" : {
         "format" : "full_html",
         "value" : "<p>Your HTML here!<>/p>"
      }
   },
   "bid" : <enter-known-block-from-d7-site>
}
```
The files created by the above processes are to be consumed by a specific, custom-build Drupal module 
whose only purpose is to ingest these data.  
  
See the custom module here:  
https://gitlab.oit.duke.edu/dlc32/dul_ingest

### dul_ingest module (custom)
```sh
$ cd /path/to/drupal9/web

# create the DUL-specific content types
$ drush dul-ctypes

# import the file_managed entities
# this needs to be done before importing nodes
$ drush file-in /path/to/file_managed

# import node entities, specifying "content-type" where appropriate
$ drush node-in /path/to/node [--content-type=rubenstein_page|datagis_page|lilly_page|music_page]

# import taxonomy and terms
$ drush tax-in /path/to/taxonomy
```

