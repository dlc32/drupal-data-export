#!/usr/bin/perl -l
=pod
=head1 Node-Export
Create JSON files that represent individual Drupal node entites.
=cut

use 5.012;
use strict;
use warnings;
use Carp;
use DBI;
use Data::Dumper;
use YAML::XS;
use JSON::XS;
use Getopt::Long;
use Digest::MD5 qw(md5);
use Config::YAML;
use File::Path qw(make_path);

use FindBin;
use lib $FindBin::Bin;
use Exhibits qw/add_exhibits_fields/;

my $file_ext = "json";

my $dbattr = {
  PrintError => 1,
  RaiseError => 0,
  AutoCommit => 0,
};

my $outdir = "$ENV{HOME}/drupal-export";

# use 'rubenstein/', 'lilly/', etc
my $url_prefix = "";

# filter clause for url_alias query
# specifically for "east" nodes (lilly or music)
my $filter_prefix = "";

# You can define your own YAML-based config and specify below.
my $c = Config::YAML->new( config => "/usr/share/drupal-data-export/global.yaml",
                           outdir => $outdir,
                           prefix => $url_prefix );

GetOptions ( "outdir|O=s" => \$outdir,
             "prefix|p=s" => \$url_prefix,
             "filter-prefix|F=s" => \$filter_prefix
           )
  or die ("Error in command line arguments\n");

my ($schema) = @ARGV;

# append 'schema' to the outdir
#$outdir .= "/" . $schema;
say "outdir=[" . $outdir . "]";
make_path( $outdir ) unless -e $outdir;

if ($url_prefix ne '') {
  $url_prefix .= "/" if $url_prefix !~ /\/$/;
}

#$filter_prefix .= "%";

# Override the default config value db.schema
# when the value is available from the CLI
$c->{db}->{schema} = $schema unless !defined $schema;

my $dsn = sprintf("dbi:%s:%s;host=%s;port=%s", 
  $c->{db}->{driver},
  $c->{db}->{schema}, 
  $c->{db}->{host},
  $c->{db}->{port});

my $db = DBI->connect($dsn, $c->{db}->{user}, $c->{db}->{password}, $dbattr)
  or die "Can't connect, yo!";

$db->{mysql_enable_utf8} = 1;

my $revision_sth = $db->prepare("SELECT * FROM node_revision WHERE nid = ? and vid = ?");

my $field_data_sth = $db->prepare("SELECT * FROM field_data_? WHERE entity_type = ? AND entity_id = ? AND revision_id = ?");
my $field_revision_sth = $db->prepare("SELECT * FROM field_revision_? WHERE entity_id ? AND revision_id = ?");

my $body_sth = $db->prepare("SELECT * FROM field_data_body WHERE entity_id = ? AND revision_id = ?");

my $tags_sth = $db->prepare("SELECT t.*, d.uuid as term_uuid FROM field_data_field_tags t "
  . "LEFT JOIN taxonomy_term_data d ON t.field_tags_tid = d.tid "
  . "WHERE t.entity_id = ? AND t.revision_id = ?"
  );

my $revision_body_sth = $db->prepare("SELECT * FROM field_revision_body WHERE entity_id = ? AND revision_id = ?");
my $revision_tags_sth = $db->prepare("SELECT * FROM field_revision_field_tags WHERE entity_id = ? AND revision_id = ?");
my $node_counter_sth = $db->prepare("SELECT * FROM node_counter WHERE nid = ?");
my $node_access_sth = $db->prepare("SELECT * FROM node_access WHERE nid = ?");
my $image_sth = $db->prepare("SELECT i.*, f.uuid as file_uuid FROM field_data_field_image i "
  . "LEFT JOIN file_managed f ON f.fid = i.field_image_fid "
  . "WHERE i.entity_type = ? AND i.entity_id = ? and i.revision_id = ?");
my $revision_image_sth = $db->prepare("SELECT * FROM field_revision_field_image WHERE entity_type = ? AND entity_id = ? and revision_id = ?");
my $url_alias_sth = $db->prepare("SELECT * FROM url_alias WHERE source = ? AND alias like ?");

# Limit to the asidebox entries that have user-entered HTML (non system/module blocks)
# We'll work on adding to the asidebox module in D7 to get at the other blocks
my $asidebox_sth = $db->prepare("SELECT * FROM asidebox WHERE nid = ? AND vid = ? AND bid > 0 AND (nid, vid) in (SELECT nid, vid FROM node where status = 1)");

# use this query when the node_type = exhibits
# my $field_data_sth = $db->prepare("SELECT * FROM field_data_? WHERE entity_type = ? AND entity_id = ? and revision_id = ?");

my $node_query = "SELECT nid, vid, type, language, title, uid, status, created, comment, promote, sticky, tnid, translate, uuid "
  . "FROM node "
  . "WHERE type NOT IN ('panel', 'bootstrap_carousel')";

my $sth = $db->prepare($node_query);
$sth->execute();

my $nodes = $sth->fetchall_arrayref({});

# -------------------------------
foreach my $n (@{$nodes}) {
  $n->{node_field_data} = {
    'type' => $n->{type},
    'langcode' => $n->{language},
    'status' => $n->{status},
    'created' => $n->{created},
    'changed' => $n->{changed},
    'promote' => $n->{promote},
    'sticky' => $n->{sticky},
    'default_langcode' => 1,
    'revision_translation_affected' => 1,
    'title' => $n->{title},
    'uid' => 1,
  };
  $n->{langcode} = $n->{language};

  # Process the 'url_alias' table
  #
  # In Drupal 7, the source followed this convention:
  # "<entity_type>/<entity_id>""
  # 
  # In Drupal 8/9, source is now "path" and the convention is:
  # "/<entity_type>/<entity_id>"
  #
  # Aliases (alias):
  # The only difference here is that aliases now start with "/" in Drupal 8/9.
  $url_alias_sth->execute("node/" . $n->{nid}, $filter_prefix . "%");
  $n->{path_alias} = $url_alias_sth->fetchrow_hashref;
  if (defined $n->{path_alias}) {
    $n->{path_alias}->{langcode} = $n->{path_alias}->{language};
    $n->{path_alias}->{alias} = "/" . $url_prefix . $n->{path_alias}->{alias};
    $n->{path_alias}->{status} = 0;
    delete $n->{path_alias}->{source};
    delete $n->{language};
  } elsif ($filter_prefix) {
    next;
  }

  # Process the BODY of the node
  $body_sth->execute($n->{nid}, $n->{vid});
  $n->{node__body} = $body_sth->fetchrow_hashref;
  if (defined $n->{node__body}) {
    $n->{node__body}->{body_format} =~ s/_no_wysiwyg//;
    $n->{node__body}->{langcode} = $n->{node__body}->{language};
    delete $n->{node__body}->{language};
    delete $n->{node__body}->{entity_type};
  }

  # Process any (and all) assigned TAGS
  $tags_sth->execute($n->{nid}, $n->{vid});
  $n->{field_tags} = $tags_sth->fetchall_arrayref({});

  foreach my $t (@{$n->{field_tags}}) {
    $t->{langcode} = $t->{language};
    $t->{field_tags_target_id} = $t->{field_tags_tid};
    delete $t->{entity_type};
    delete $t->{field_tags_tid};
  }

  # This may be ignored as a Drupal 9 equivalent could not 
  # be identified
  # DEPRECATE (node_counter is a 3rd party module / not part of Drupal core)
  #my $nc_res = $node_counter_sth->execute($n->{nid});
  #$n->{counter} = 0;
  #$n->{counter} = $node_counter_sth->fetchrow_hashref unless !defined $nc_res;

  # Process the NODE_ACCESS row
  $node_access_sth->execute($n->{nid});
  $n->{node_access} = $node_access_sth->fetchrow_hashref;
  if (defined $n->{node_access}) {
    delete $n->{node_access}->{fallback};
    $n->{node_access}->{langcode} = $n->{language};
  }

  # Process the node's associated IMAGE
  $image_sth->execute('node', $n->{nid}, $n->{vid});
  $n->{field_image} = $image_sth->fetchrow_hashref;
  if (defined $n->{field_image}) {
    $n->{field_image}->{langcode} = $n->{field_image}->{language};
    $n->{field_image}->{field_image_target_id} = $n->{field_image}->{field_image_fid};
    delete $n->{node__field_image}->{language};
    delete $n->{node__field_image}->{field_image_fid};
  }

  # ASIDEBOX migration per node
  $asidebox_sth->execute($n->{nid}, $n->{vid});
  $n->{asidebox} = $asidebox_sth->fetchrow_hashref;

  # process the most recent revision
  $revision_sth->execute($n->{nid}, $n->{vid});
  $n->{revisions} = $revision_sth->fetchall_arrayref({});

  foreach my $r (@{$n->{revisions}}) {
    $r->{field_revision} = {
      'langcode' => $r->{language},
      'status' => $r->{status},
      'uid' => 1,
      'title' => $r->{title},
      'created' => $r->{created},
      'changed' => $r->{changed},
      'sticky' => $r->{sticky},
      'default_langcode' => 1,
      'revision_translation_affected' => 1,
    };

    $revision_body_sth->execute($r->{nid}, $r->{vid});
    $r->{node_revision__body} = $revision_body_sth->fetchrow_hashref;
    $r->{node_revision__body}->{langcode} = $r->{node_revision__body}->{language};
    delete $r->{node_revision__body}->{language};
    delete $r->{node_revision__body}->{entity_type};

    $revision_tags_sth->execute($r->{nid}, $r->{vid});
    $r->{node_revision__field_tags} = $revision_tags_sth->fetchall_arrayref({});
    foreach my $t (@{$r->{node_revision__field_tags}}) {
      $t->{langcode} = $t->{language};
      $t->{field_tags_target_id} = $t->{field_tags_tid};
      delete $t->{entity_type};
      delete $t->{field_tags_tid};
    }
    delete $r->{nid};
    delete $r->{vid};
  }

  if ($n->{type} eq 'exhibit') {
    add_exhibits_fields $n, $db;
  }

  # Remove the fields (hash keys) that are not to be 
  # included in the YAML output.
  delete $n->{title};
  delete $n->{language};
  delete $n->{uid};
  delete $n->{status};
  delete $n->{created};
  delete $n->{changed};
  delete $n->{promote};
  delete $n->{sticky};
  delete $n->{tnid};
  delete $n->{translate};
  delete $n->{comment};
}
# -------------------------------

# configure our JSON object to:
# - print "pretty" structured output
# - with sorted keys
# - enforce UTF8
my $json = JSON::XS->new->pretty(1)->utf8->space_after->canonical(1);

# ensure the 'node' directory is present and empty 
# before saving files.
mkdir "$outdir/node" if !-e "$outdir/node";
`rm $outdir/node/*` if -e "$outdir/node";

# remove the '%' added to the end of filter_prefix

foreach my $z (@{$nodes}) {

  # if we're filtering by alias prefix, move on to the next
  # entry if alias is uninitialized
  next if !$z->{path_alias}->{alias} && $filter_prefix;
  #say "alias = " . $z->{path_alias}->{alias} . "; filter_prefix = " . $filter_prefix;
  next if $z->{path_alias}->{alias} && ($z->{path_alias}->{alias} !~ /^\/$filter_prefix/);

  say $z->{path_alias}->{alias};
  my $fname = sprintf("%s.%s", $z->{uuid}, $file_ext);

  my $fullpath = sprintf("%s/node/%s", $outdir, $fname);
  open my $fh, '>', $fullpath or die "Unable to open file $fullpath: $!\n";
  print $fh $json->encode( $z );
}
# print Dump $nodes->[0];

# "FINISH" all of the prepared statement handles before
# disconnecting the database handle.
$body_sth->finish;
$revision_sth->finish;
$revision_tags_sth->finish;
$revision_body_sth->finish;
$node_counter_sth->finish;
$node_access_sth->finish;
$tags_sth->finish;
$url_alias_sth->finish;
$revision_image_sth->finish;
$image_sth->finish;
$asidebox_sth->finish;
$field_data_sth->finish;

$db->disconnect;

__END__
