#!/usr/bin/perl

use 5.012;
use strict;
use warnings;
use DBI;
use Data::Dumper;
use YAML::XS;
use JSON::XS;
use Getopt::Long;
use Digest::MD5 qw(md5);
use Config::YAML;
use File::Path qw(make_path);

my $dbattr = {
  PrintError => 1,
  RaiseError => 0,
  AutoCommit => 0,
};

my $outdir = "$ENV{HOME}/drupal-export";

# use 'block'
my $module = "block";

# You can define your own YAML-based config and specify below.
my $c = Config::YAML->new( config => "/usr/share/drupal-data-export/global.yaml",
                           outdir => $outdir,
                           module => $module );

GetOptions ( "outdir|O=s" => \$outdir,
             "module|m=s" => \$module )
  or die ("Error in command line arguments\n");

my ($schema) = @ARGV;

# append 'schema' to the outdir
# $outdir .= "/" . $schema;
say "outdir=[" . $outdir . "]";
make_path( $outdir ) unless -e $outdir;
#mkdir $outdir unless -e $outdir;

# Override the default config value db.schema
# when the value is available from the CLI
$c->{db}->{schema} = $schema unless !defined $schema;

my $dsn = sprintf("dbi:%s:%s;host=%s;port=%s", 
  $c->{db}->{driver},
  $c->{db}->{schema}, 
  $c->{db}->{host},
  $c->{db}->{port});

my $db = DBI->connect($dsn, $c->{db}->{user}, $c->{db}->{password}, $dbattr)
  or die "Can't connect, yo!";

$db->{mysql_enable_utf8} = 1;

# Fetch and export all of the known custom blocks
# that have content in its "body" field.
# -------
#my $custom_blocks_sth = $db->prepare("SELECT bc.*, b.delta, b.title as block_title, "
#    . "b.pages as block_pages, b.status as block_status, "
#    . "b.weight as block_weight, b.region as block_region, "
#    . "b.module as block_module, "
#    . "b.visibility as block_visibility, b.delta as block_delta "
#    . "FROM block b "
#    . "RIGHT JOIN block_custom bc ON bc.bid = b.bid "
#    . "WHERE module in ('block', 'menu') "
#    );

my $custom_blocks_sth = $db->prepare("SELECT * FROM block_custom");
$custom_blocks_sth->execute;
my $blocks = $custom_blocks_sth->fetchall_arrayref({});

# ensure the 'node' directory is present and empty 
# before saving files.
mkdir "$outdir/blocks" if !-e "$outdir/blocks";
mkdir "$outdir/blocks/custom" if !-e "$outdir/blocks/custom";
`rm $outdir/blocks/custom/*` if -e "$outdir/blocks/custom";

# configure our JSON object to:
# - print "pretty" structured output
# - with sorted keys
# - enforce UTF8
my $json = JSON::XS->new->pretty(1)->utf8->space_after->canonical;

my $blocks_exported = 0;
foreach (@{$blocks}) {
  my $b = {
    'bid' => $_->{bid},
    'content' => {
      'info' => $_->{info},
      'body' => { 'value' => $_->{body}, 'format' => $_->{format}, },
      'type' => 'basic',
      'langcode' => 'en',
      'uuid' => '%UUID%'
    },
  };
  # $b->{content}->{type} = 'menu' if $_->{block_module} eq 'menu';

  # say Dumper $_;
  my $fullpath = sprintf("%s/blocks/custom/block-custom-%s.json", $outdir, $_->{bid});
  say $fullpath;
  open my $fh, '>', $fullpath or die "Unable to open file $fullpath: $!\n";
  print $fh $json->encode( $b );
  $blocks_exported++;
  #print $fh Dump { term => $t };
}

say sprintf("%s custom blocks exported...", $blocks_exported);
$db->disconnect; 
__END__
