package Exhibits;

use 5.012;
use strict;
use warnings;
use Data::Dumper;
use DBI;

use Exporter qw(import);
our @EXPORT_OK = qw(add_exhibits_fields);

# These are the fields attached to an exhibit node.
# In the database, the table naming convention is:
# field_data_XXXX and field_revision_XXXX
# (where XXXX is one the below values)
our @FIELDS = (
  'field_exhibit_artist',
  'field_exhibit_contact',
  'field_exhibit_image_caption',
  'field_exhibit_location',
  'field_exhibit_location_list',
  'field_exhibit_online_exhibit',
  'field_exhibit_sponsor',
  'field_exhibit_sub_title',
  'field_external_url',
  'field_opening_date',
  'field_rubenstein_exhibit_',
);

sub add_exhibits_fields {
  my $node = shift;
  my $db = shift;

  # All "field" tables in Drupal have common fields (language, entity_id, revision_id, bundle, delta, deleted and entity_id
  my $q = "SELECT * FROM field_revision_%s WHERE entity_type = 'node' AND bundle = 'exhibit' AND entity_id = ? AND revision_id = ? ORDER BY revision_id DESC LIMIT 1";

  $node->{exhibit} = {};

  foreach my $field_name (@FIELDS) {
    # use sprintf to substitute '%s' with the field_name to 
    # satisty the table naming convention
    my $st = $db->prepare(sprintf($q, $field_name));

    # execute the query, passing nid (node_id) and vid (revision_id)
    $st->execute($node->{nid}, $node->{vid});

    # fetch the row as a hash reference (HASHREF);
    my $row = $st->fetchrow_hashref;

    # move on to the next field if this one has no data!
    next if !defined($row);

    # 'langcode' (Drupal 9) is the same as 'language' (Drupal 7)
    # $row->{langcode} = $row->{language};

    # remove the attributes (keys) that don't matter
    delete $row->{entity_type};
    delete $row->{bundle};
    delete $row->{entity_id};
    delete $row->{revision_id};
    delete $row->{language};
    delete $row->{deleted};
    delete $row->{langcode};
    delete $row->{delta};

    my $d = {};
    foreach (keys %{$row}) {
      # PERL newbies:
      # NOTE: the current item is stored in $_

      # ignore these guys...
      # DEPRECATED NOW --> next if /(deleted|langcode|delta)/;

      my $key = $_;
      # remove the 'field_XXXX' from this key
      # leaving just the actual attribute
      # (i.e. value or format or etc...)

      # for Perl newbies, the substitute operator (s//) 
      # operates on the $_ variable unless a user-defined variable 
      # is specified.
      s/$field_name//;
      s/^_//;
      $d->{$_} = $row->{$key};

      #$row->{$_} = $row->{$key};
      delete $row->{$key};
    }

    #$node->{exhibit}->{$field_name} = $row;
    $node->{exhibit}->{$field_name} = $d;
  }
}

1;
