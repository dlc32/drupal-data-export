#!/usr/bin/perl

use 5.012;
use strict;
use warnings;
use DBI;
use Data::Dumper;
use JSON::XS;
use Getopt::Long;
use Digest::MD5 qw(md5);
use Config::YAML;
use File::Path qw(make_path);

my $dbattr = {
  PrintError => 1,
  RaiseError => 0,
  AutoCommit => 0,
};

my $outdir = "$ENV{HOME}/drupal-export";

# use 'rubenstein/', 'lilly/', etc
my $url_prefix = "";

# You can define your own YAML-based config and specify below.
my $c = Config::YAML->new( config => "/usr/share/drupal-data-export/global.yaml",
                           outdir => $outdir,
                           prefix => $url_prefix );

GetOptions ( "outdir|O=s" => \$outdir,
             "prefix|p=s" => \$url_prefix )
  or die ("Error in command line arguments\n");

my ($schema) = @ARGV;

# append 'schema' to the outdir
# $outdir .= "/" . $schema;
say "outdir=[" . $outdir . "]";
make_path( $outdir ) unless -e $outdir;
#mkdir $outdir unless -e $outdir;

# Override the default config value db.schema
# when the value is available from the CLI
$c->{db}->{schema} = $schema unless !defined $schema;

my $dsn = sprintf("dbi:%s:%s;host=%s;port=%s", 
  $c->{db}->{driver},
  $c->{db}->{schema}, 
  $c->{db}->{host},
  $c->{db}->{port});

my $db = DBI->connect($dsn, $c->{db}->{user}, $c->{db}->{password}, $dbattr)
  or die "Can't connect, yo!";

$db->{mysql_enable_utf8} = 1;

my $json = JSON::XS->new->pretty(1)->utf8->space_after;

# Fetch and export all of the known taxonomy terms
# including the config name for the assigned vocabulary
my $files_sth = $db->prepare("SELECT * FROM file_managed");

$files_sth->execute;
my $managed_files = $files_sth->fetchall_arrayref({});

# ensure the 'file_managed' directory is present and empty 
# before saving files.
mkdir "$outdir/file_managed" if !-e "$outdir/file_managed";
`rm $outdir/file_managed/*` if -e "$outdir/file_managed";

foreach my $f (@{$managed_files}) {
  say $f->{uuid} . ".json";
  my $fullpath = sprintf("%s/file_managed/%s.json", $outdir, $f->{uuid});
  open my $fh, '>', $fullpath or die "Unable to open file $fullpath: $!\n";
  print $fh $json->encode( $f );
}

$db->disconnect; 
__END__
